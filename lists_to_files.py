string1 = '128,129', '134, 167', '127,189'
string2 = '154, 134', '156, 124', '138, 196'
l1 = list(string1)
l2 = list(string2)
data = [l1, l2]
f = open("data.txt", "w")
for val in data:
    f.write(str(val))
    f.write('\n')
f.close()
# open data, read each item to new line in file, delete blank lines
with open("data.txt", "r", encoding='utf-8') as f:
    reader = f.read()
    file = reader.split('\n')
    print(reader)
    file = filter(None, file)

    # create one file for each list of data (1) on every new line 
    i = 0
    for v in file:
        i += 1
        f = open("data_%s.txt" % i, 'w')
        f.write(v)
    f.close()
    '''
    # try a list comprehension
    y = len()
    #files = [open("data%s.txt" % i, "w") for i in y]
    print(y)
    #for fs in files:
    #    [fs.write(value) for value in file]
    '''
    # enumerate is shorter than counting
    for i, line in enumerate(file):
        fi = open("data%s.txt" % i, "w")
        fi.write(line)
    f.close()
